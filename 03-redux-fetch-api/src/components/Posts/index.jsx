import { useEffect } from 'react'
import { useSelector,useDispatch } from 'react-redux'

import { selectAllPosts, getPostsStatus, getPostsError, fetchPosts } from '../../store/modules/posts/postSlice'
import PostsExcerpt from '../PostsExcerpt'
import AddPostForm from './AddPostForm'

import './posts.css'

const PostsList = () => {
  const dispatch = useDispatch()

  const posts = useSelector(selectAllPosts)
  const postStatus = useSelector(getPostsStatus)
  const error = useSelector(getPostsError)

  let content
    if (postStatus === 'loading') {
        content = <p>"Loading..."</p>;
    } else if (postStatus === 'succeeded') {
        const orderedPosts = posts.slice().sort((a, b) => b.date.localeCompare(a.date))
        content = orderedPosts.map(post => <PostsExcerpt key={post.id} post={post} />)
    } else if (postStatus === 'failed') {
        content = <p>{error}</p>;
    }

  return (
    <section className="postSection">
      <AddPostForm />
      <h2>Posts</h2>
      {content}
    </section>
  )
}

export default PostsList