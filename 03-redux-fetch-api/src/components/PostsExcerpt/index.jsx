
import TimeAgo from '../TimeAgo'
import UserAuthor from '../UsersAuthor'
import ReactionButtons from '../ReactionButtons'

import './excerpt.css'

const PostsExcerpt = ({ post }) => {
  return (
    <article className="postArticle">
      <h3>{post.title}</h3>
      <p>{post.body.substring(0, 100)}</p>
      <p>
        <UserAuthor userId={post.userId} />
        <TimeAgo timestamp={post.date} />
      </p>
      <ReactionButtons post={post} />
    </article>
  )
}

export default PostsExcerpt