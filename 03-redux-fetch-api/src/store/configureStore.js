import { configureStore } from "@reduxjs/toolkit";
import postsReducer from "./modules/posts/postSlice";
import usersReducer from "./modules/users/usersSlice";

export const store = configureStore({
  reducer: {
    posts: postsReducer,
    users: usersReducer
  }
})