import { Route, Routes } from 'react-router-dom'
import DefaultLayout from './Layout/DefaultLayout'
import Home from './pages/Home'
import Post from './pages/Posts'
import AddPost from './pages/Posts/components/AddPost'
import EditPost from './pages/Posts/components/EditPost'

export const Router = () => {
  return (
    <Routes>
      <Route path="/" element={<DefaultLayout />}>
        <Route path="/" element={<Home />} />
        {/* <Route path="*" element={<Home />} /> */}

        <Route path="post">
          <Route path=":postId" element={<Post />} />
          <Route path="edit/:postId" element={<EditPost />} />
          <Route path="add" element={<AddPost />} />
        </Route>
      </Route>
    </Routes>
  )
}
