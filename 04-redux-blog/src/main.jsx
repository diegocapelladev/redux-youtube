import React from 'react'
import ReactDOM from 'react-dom/client'
import { MyThemeProvider } from './styles/MyThemeProvider'
import App from './App'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import { store } from './store/store'
import { fetchPosts } from './store/modules/post/postSlice'
import { fetchUsers } from './store/modules/user/userSlice'

store.dispatch(fetchPosts())
store.dispatch(fetchUsers())

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <Provider store={store}>
      <MyThemeProvider>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </MyThemeProvider>
    </Provider>
  </React.StrictMode>
)
