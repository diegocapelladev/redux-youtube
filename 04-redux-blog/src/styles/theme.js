export const theme = {
  grid: {
    container: '112rem',
    gutter: '3.2rem'
  },
  border: {
    radius: '0.6rem'
  },
  font: {
    family:
      "Poppins, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
    light: 300,
    regular: 400,
    medium: 500,
    semiBold: 600,
    bold: 700,
    black: 800,
    sizes: {
      size48: '4.8rem',
      size32: '3.2rem',
      size24: '2.4rem',
      size20: '2rem',
      size18: '1.8rem',
      size16: '1.6rem',
      size14: '1.4rem',
      size12: '1.2rem',
      size10: '1rem'
    }
  },
  colors: {
    background: '#071422',
    profile: '#0B1B2B',
    input: '#040F1A',
    post: '#112131',
    border: '#1C2F41',
    label: '#3A536B',
    span: '#7B96B2',
    text: '#AFC2D4',
    subtitle: '#C4D4E3',
    title: '#E7EDF4',
    white: '#FAFAFA',
    black: '#030517',
    blue: '#3294F8',
    error: '#AB222E'
  },
  spacings: {
    xxsmall: '0.8rem',
    xsmall: '1.6rem',
    small: '2.4rem',
    medium: '3.2rem',
    large: '4.0rem',
    xlarge: '4.8rem',
    xxlarge: '5.6rem',
    huge: '7.2rem'
  }
}
