import { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate, useParams } from 'react-router-dom'

import {
  deletePost,
  selectPostById,
  updatePost
} from '../../../../store/modules/post/postSlice'
import { selectAllUsers } from '../../../../store/modules/user/userSlice'
import * as S from './styles'

const AddPost = () => {
  const users = useSelector(selectAllUsers)
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const { postId } = useParams()

  const post = useSelector((state) => selectPostById(state, Number(postId)))

  const [title, setTitle] = useState(post?.title)
  const [content, setContent] = useState(post?.body)
  const [userId, setUserId] = useState(post?.userId)
  const [requestStatus, setRequestStatus] = useState('idle')

  if (!post) {
    return (
      <S.Wrapper>
        <h1>Post not found!</h1>
      </S.Wrapper>
    )
  }

  const onTitleChanged = (e) => setTitle(e.target.value)
  const onContentChanged = (e) => setContent(e.target.value)
  const onAuthorChanged = (e) => setUserId(Number(e.target.value))

  const canSave =
    [title, content, userId].every(Boolean) && requestStatus === 'idle'

  const onSavePost = () => {
    if (canSave) {
      try {
        setRequestStatus('loading')
        dispatch(
          updatePost({
            id: post.id,
            title,
            body: content,
            userId,
            reactions: post.reactions
          })
        ).unwrap()

        setTitle('')
        setContent('')
        setUserId('')
        navigate(`/post/${postId}`)
      } catch (error) {
        console.error('Failed to save the post!', error)
      } finally {
        setRequestStatus('idle')
      }
    }
  }

  const usersOptions = users.map((user) => (
    <option key={user.id} value={user.id}>
      {user.name}
    </option>
  ))

  const onDeletePost = () => {
    try {
      setRequestStatus('loading')
      dispatch(deletePost({ id: post.id })).unwrap()

      setTitle('')
      setContent('')
      setUserId('')
      navigate('/')
    } catch (error) {
      console.error('Failed to delete the Post.', error)
    } finally {
      setRequestStatus('idle')
    }
  }

  return (
    <S.Wrapper>
      <h1>Edit Post</h1>
      <S.Form>
        <S.Label htmlFor="postTitle">Post Title:</S.Label>
        <S.Input
          type="text"
          id="postTitle"
          name="postTitle"
          value={title}
          onChange={onTitleChanged}
          placeholder="Post title"
        />

        <S.Label htmlFor="postAuthor">Author:</S.Label>
        <S.Select
          type="text"
          id="postAuthor"
          name="postAuthor"
          value={userId}
          onChange={onAuthorChanged}
        >
          <option value="">Select...</option>
          {usersOptions}
        </S.Select>

        <S.Label htmlFor="postContent">Content:</S.Label>
        <S.TextArea
          type="text"
          id="postContent"
          name="postContent"
          value={content}
          onChange={onContentChanged}
          cols="30"
          rows="10"
          placeholder="Your message..."
        />

        <S.SaveButton type="button" onClick={onSavePost} disabled={!canSave}>
          Save Post
        </S.SaveButton>
        <S.DeleteButton
          type="button"
          onClick={onDeletePost}
          disabled={!canSave}
        >
          Delete Post
        </S.DeleteButton>
      </S.Form>
    </S.Wrapper>
  )
}

export default AddPost
