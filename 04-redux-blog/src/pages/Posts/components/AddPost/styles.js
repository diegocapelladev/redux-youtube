import styled, { css } from 'styled-components'
import media from 'styled-media-query'

export const Wrapper = styled.main`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  margin: 4rem 0;
`

export const Form = styled.form`
  margin-top: 4rem;
  display: flex;
  flex-direction: column;
  gap: 1rem;
  max-width: 50%;
  width: 100%;

  ${media.lessThan('large')`
    max-width: 100%;
  `}
`

export const Label = styled.label`
  ${({ theme }) => css`
    color: ${theme.colors.subtitle};
    font-size: ${theme.font.sizes.size18};
  `}
`

const BaseLayout = css`
  ${({ theme }) => css`
    font-size: ${theme.font.sizes.size16};
    color: ${theme.colors.subtitle};
    background: ${theme.colors.input};
    border: 1px solid ${theme.colors.border};
    border-radius: ${theme.border.radius};
    padding: 1rem;
    margin-bottom: 2rem;

    &:focus {
      outline: none;
      border: 1px solid ${theme.colors.blue};
    }

  `}
`

export const Input = styled.input`
  ${({ theme }) => css`
    ${BaseLayout}

    &::placeholder {
      color: ${theme.colors.label};
      font-size: ${theme.font.sizes.size16};
    }
  `}
`

export const Select = styled.select`
  ${BaseLayout}
  cursor: pointer;
`

export const TextArea = styled.textarea`
  ${({ theme }) => css`
    ${BaseLayout}
    resize: none;

    &::placeholder {
      color: ${theme.colors.label};
      font-size: ${theme.font.sizes.size16};
    }
  `}
`
export const SaveButton = styled.button`
  ${({ theme }) => css`
    background: ${theme.colors.blue};
    font-size: ${theme.font.sizes.size16};
    font-weight: 600;
    color: ${theme.colors.subtitle};
    border: 1px solid ${theme.colors.border};
    border-radius: ${theme.border.radius};
    padding: 1rem;
    cursor: pointer;

    &:disabled  {
      background: ${theme.colors.label};
      cursor: not-allowed;
    }
  `}
`
