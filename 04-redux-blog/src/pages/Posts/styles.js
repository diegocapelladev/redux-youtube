import styled, { css } from 'styled-components'
import media from 'styled-media-query'

export const Section = styled.section`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 7.2rem;
`
export const TitleDetails = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 4rem 0;
`

export const Wrapper = styled.main`
  ${({ theme }) => css`
    font-size: ${theme.font.sizes.size18};
    background: ${theme.colors.post};
    padding: 3.2rem;
    border-radius: 1rem;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    gap: 2rem
    width: 100%;
    position: relative;

    a {
      text-decoration: none;
      position: absolute;
      right: 0;
      top: 0;
      display: block;
      background: ${theme.colors.label};
      padding: 0.4rem 0.8rem;
      border-top-right-radius: 0.6rem;
      border-bottom-left-radius: 0.6rem;
      transition: all 0.2s;

      svg {
        color: ${theme.colors.blue};

        &:hover {
          color: ${theme.colors.white};
        }
      }

      ${media.lessThan('medium')`
        opacity: 0.5;
      `}
    }

    ${media.lessThan('large')`
      gap: 1rem;
      padding: 1.6rem;
    `}
  `}
`
export const Content = styled.div`
  display: flex;
  flex-direction: column;
  gap: 2rem;
  ${({ theme }) => css`

    ${media.lessThan('medium')`

      h2 {
        font-size: ${theme.font.sizes.size18};
      }

      p {
        font-size: ${theme.font.sizes.size14};
      }
    `}
  `}
`

export const Title = styled.h2`
  ${({ theme }) => css`
    font-size: ${theme.font.sizes.size20};
    font-weight: 700;
  `}
`

export const Description = styled.p`
  ${({ theme }) => css`
    font-size: ${theme.font.sizes.size16};
  `}
`
export const Info = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin: 2rem 0;

  span {
    ${({ theme }) => css`
    font-style: italic;
    font-size: ${theme.font.sizes.size14};
    color: ${theme.colors.span};
  `}
  }
`
