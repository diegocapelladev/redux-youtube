import { useSelector } from 'react-redux'

import PostItem from '../../components/PostItem'
import {
  getPostsError,
  getPostsStatus,
  selectAllPosts
} from '../../store/modules/post/postSlice'
import * as S from './styles'

const Home = () => {
  const posts = useSelector(selectAllPosts)
  const postStatus = useSelector(getPostsStatus)
  const error = useSelector(getPostsError)

  let content
  if (postStatus === 'success') {
    const orderedPosts = posts
      .slice()
      .sort((a, b) => b.date.localeCompare(a.date))
    content = orderedPosts.map((post) => <PostItem key={post.id} post={post} />)
  } else if (postStatus === 'failed') {
    content = <p>{error}</p>
  }

  return (
    <>
      {postStatus === 'loading' ? (
        <S.Title>
          <h1>Loading...</h1>
        </S.Title>
      ) : (
        <>
          <S.Title>
            <h1>Blog Posts</h1>
          </S.Title>
          <S.Wrapper>{content}</S.Wrapper>
        </>
      )}
    </>
  )
}

export default Home
