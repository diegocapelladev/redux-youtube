import { configureStore } from '@reduxjs/toolkit'
import postsReducer from './modules/post/postSlice'
import usersReducer from './modules/user/userSlice'

export const store = configureStore({
  reducer: {
    posts: postsReducer,
    users: usersReducer
  }
})
