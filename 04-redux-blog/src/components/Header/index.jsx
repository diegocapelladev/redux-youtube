import { Link } from 'react-router-dom'

import logo from '/images/terminal.png'
import * as S from './styles'

const Header = () => (
  <S.Wrapper>
    <S.HeaderContainer>
      <Link to="/">
        <img src={logo} alt="" />
      </Link>

      <S.Navbar>
        <Link to="/">Home</Link>

        <Link to="/post/add">Add Post</Link>
      </S.Navbar>
    </S.HeaderContainer>
  </S.Wrapper>
)

export default Header
