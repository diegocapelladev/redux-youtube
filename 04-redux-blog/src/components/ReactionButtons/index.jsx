import { useDispatch } from 'react-redux'

import { reactionAdded } from '../../store/modules/post/postSlice'
import * as S from './styles'

const reactionEmoji = {
  thumbsUp: '👍',
  wow: '😮',
  heart: '❤️',
  rocket: '🚀',
  coffee: '☕'
}

const ReactionButtons = ({ post }) => {
  const dispatch = useDispatch()

  const reactionButtons = Object.entries(reactionEmoji).map(([name, emoji]) => {
    return (
      <S.Button
        key={name}
        type="button"
        onClick={() =>
          dispatch(reactionAdded({ postId: post.id, reaction: name }))
        }
      >
        {emoji} <span></span> {post.reactions[name]}
      </S.Button>
    )
  })

  return <S.Wrapper>{reactionButtons}</S.Wrapper>
}

export default ReactionButtons
