import { useDispatch } from "react-redux"
import {reactionAdded} from '../../store/modules/posts/postSlice'

const reactionEmoji = {
  thumbsUp: '👍',
  wow: '😮',
  heart: '❤️',
  rocket: '🚀',
  coffee: '☕'
}

const ReactionButtons = ({ post }) => {
  const dispatch = useDispatch()

  const reactionButtons = Object.entries(reactionEmoji).map(([ name, emoji ]) => {
    return (
      <button
        key={name}
        type="button"
        className="reactionButtons"
        onClick={() => dispatch(reactionAdded({ postId: post.id, reaction: name }))}
      >
        {emoji} {post.reactions[name]}
      </button>
    )
  })

  return (
    <div>{reactionButtons}</div>
  )
}

export default ReactionButtons