import { useSelector } from 'react-redux'

import { selectAllPosts } from '../../store/modules/posts/postSlice'
import ReactionButtons from '../ReactionButtons'
import TimeAgo from '../TimeAgo'
import UserAuthor from '../UsersAuthor'
import AddPostForm from './AddPostForm'
import './posts.css'

const PostsList = () => {
  const posts = useSelector(selectAllPosts)

  const orderedPosts = posts.slice().sort((a, b) => b.date.localeCompare(a.date))

  const renderedPosts = orderedPosts.map((post) => {
    return (
      <article key={post.id} className="postArticle">
        <h3>{post.title}</h3>
        <p>{post.content.substring(0, 100)}</p>
        <p>
          <UserAuthor userId={post.userId} />
          <TimeAgo timestamp={post.date} />
        </p>
        <ReactionButtons post={post} />
      </article>
    )
  })

  return (
    <section className="postSection">
      <h2>Posts</h2>
      <AddPostForm />
      {renderedPosts}
    </section>
  )
}

export default PostsList