import { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { decrement, increment, reset, incrementByAmount } from './store/counterSlice'

import './App.css'

function App() {
  const dispatch = useDispatch()
  const { count } = useSelector((state) => state.counter)

  const [ incrementAmount, setIncrementAmount ] = useState(0)

  const addValue = Number(incrementAmount) || 0

  const resetAll = () => {
    setIncrementAmount(0)
    dispatch(reset())
  }

  return (
    <div className="App">
      <h1>Total: {count}</h1>

      <div className='Input'>
        <input
          type="number" 
          value={incrementAmount} 
          onChange={(e) => setIncrementAmount(e.target.value)} 
        />
        <button 
          onClick={() => dispatch(incrementByAmount(addValue))} 
        >Add By Amount</button>
        
      </div>

      <div style={{ display: 'flex', gap: '20px' }}>
        <button onClick={() => dispatch(increment())}>Increment</button>
        <button onClick={() => dispatch(decrement())}>Decrement</button>
        <button onClick={() => dispatch(reset())}>Reset</button>
      </div>

      <button onClick={resetAll}>Reset All</button>
    </div>
  ) 
}

export default App
